# configure.ac - configure script configurations
#
# This file is a part of Grace.
# Copyright (C) 2021 Akib Azmain Turja
#
# Grace is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Grace is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

AC_PREREQ(2.59)
AC_INIT([grace], [0.0.6])

PACKAGE_NAME_PRETTY="Grace"
AC_SUBST([PACKAGE_NAME_PRETTY], [${PACKAGE_NAME_PRETTY}])

AC_CANONICAL_SYSTEM

# Check the architecture and configure accordingly
AC_MSG_CHECKING([host architecture])
arch="$(echo "$host" | sed 's/-/ /' | awk '{print $1}')"
AC_MSG_RESULT([${arch}])

supported_arches=$(
cat << "EOF"
    i386
    i486
    i586
    i686
EOF
)

AC_MSG_CHECKING([whether the architecture is supported])
AS_IF([! grep -q "$arch" <<< "${supported_arches}"],
[
    AC_MSG_RESULT([no])
    AC_MSG_ERROR([

--------------------------------------------------------------------------------
Sorry, Grace doesn't support '${arch}' architecture right now.

Currently supported architectures are:

${supported_arches}

Grace is now under heavy development, so many architectures are not supported.
Support for those architectures may be added in future. You can wait for Grace
to support your architecture or help to port Grace to your architecture.
--------------------------------------------------------------------------------])
])
AC_MSG_RESULT([yes])

# Make 'arch' variable available to 'make'
AC_SUBST([arch], [${arch}])

AM_INIT_AUTOMAKE()

AC_CONFIG_MACRO_DIRS([m4])

AC_PROG_CXX
AC_PROG_CC
AM_PROG_AS

AC_CONFIG_FILES(
    makefile
    src/makefile
    docs/makefile
)

AC_ARG_ENABLE(
    [building-docs], [  --enable-building-docs  build documentations for Grace (default: yes)],
    [build_docs=${enableval}], [build_docs=yes]
)

AC_ARG_WITH(
    [docs-theme], [  --with-docs-theme[[=theme]]    sphinx theme to use for building documentations],
    [SPHINXTHEME=${withval}], [SPHINXTHEME=sphinx_rtd_theme]
)

AS_IF([test ! -z ${build_docs} && test ${build_docs} = yes],
[
    DOXYGEN=
    SPHINXBUILD=
    SPHINXFLAGS=" "

    AC_ARG_VAR([DOXYGEN], [path to doxygen for building documentations])
    AC_ARG_VAR([SPHINXBUILD], [path to sphinx-build for building documentations])
    AC_ARG_VAR([SPHINXFLAGS], [flags for sphinx-build])

    AC_MSG_CHECKING([for doxygen])
    AS_IF([test -z ${DOXYGEN}], [DOXYGEN=$(which doxygen >/dev/null && echo doxygen)])
    AS_IF([test ! -z ${DOXYGEN}],
    [
        AC_MSG_RESULT([${DOXYGEN}])

        AC_MSG_CHECKING([for sphinx-build])
        AS_IF([test -z ${SPHINXBUILD}], [SPHINXBUILD=$(which sphinx-build >/dev/null && echo sphinx-build)])
        AS_IF([test ! -z "${SPHINXBUILD}"],
        [
            AC_MSG_RESULT([${SPHINXBUILD}])

            AC_MSG_CHECKING([whether sphinx is able to build documentations])

            mkdir .sphinx-test
            mkdir .sphinx-test/build

            echo "
project = 'Sphinx Test'
extensions = [[
    'breathe'
]]
html_theme = '${SPHINXTHEME}'
" > .sphinx-test/conf.py

            echo "
Sphinx Test
===========

This build is to test Sphinx.
" > .sphinx-test/index.rst
            AS_IF([sphinx-build .sphinx-test/ .sphinx-test/build -Q >/dev/null 2>&1],
            [
                AC_MSG_RESULT(yes)

                AC_CONFIG_FILES(
                    docs/conf.py
                    docs/doxyfile
                )
                AC_SUBST([DOXYGEN], [${DOXYGEN}])
                AC_SUBST([SPHINXBUILD], [${SPHINXBUILD}])
                AC_SUBST([SPHINXFLAGS], [${SPHINXFLAGS}])
                AC_SUBST([SPHINXTHEME], [${SPHINXTHEME}])
                AM_CONDITIONAL([build_docs], [true])
            ],
            [
                AC_MSG_RESULT([no])
                AC_MSG_WARN(
                    [sphinx isn't able to build documentations correctly, documentations won't be build]
                )
                unset SPHINXBUILD
                unset SPHINXFLAGS
                unset DOXYGEN
                unset build_docs
                unset SPHINXTHEME
            ])

            rm -rf .sphinx-test
        ],
        [
            AC_MSG_RESULT([])
            AC_MSG_WARN([can't find sphinx-build, documentations won't be build])
            unset DOXYGEN
            unset build_docs
            unset SPHINXTHEME
        ])
    ],
    [
        AC_MSG_RESULT([])
        AC_MSG_WARN([can't find doxygen, docs won't be build])
        unset build_docs
        unset SPHINXTHEME
    ])
])

AC_OUTPUT

echo -n "
--------------------------------------------------------------------------------

Grace was configured with the following configuration:

    Target architecture: ${arch}
    Target triplet: ${host}

    CC: ${CC}
    CXX: ${CXX}
    CCAS: ${CCAS}
    CFLAGS: ${CFLAGS}
    CXXFLAGS: ${CXXFLAGS}
    CCASFLAGS: ${CCASFLAGS}
    CPPFLAGS: ${CPPFLAGS}

    Build documentations: ${build_docs:-no}
    DOXYGEN: ${DOXYGEN:-N/A}
    SPHINXBUILD: ${SPHINXBUILD:-N/A}
    SPHINXFLAGS: ${SPHINXFLAGS:-N/A}
    Documentations theme: ${SPHINXTHEME:-N/A}

Now execute 'make' to build Grace. If you have QEMU installed, you can run the
kernel with:

    qemu-system-i386 -kernel src/grace

If you want to change the configuration, execute '$0' again with
appropriate arguments.
"
