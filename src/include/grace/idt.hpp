/*  idt.hpp - IDT related functions and structures for i386 architecture

    This file is a part of Grace.
    Copyright (C) 2021 Akib Azmain Turja

    Grace is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Grace is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <stdint.h>
#include <stddef.h>

namespace grace
{
    namespace idt
    {
        struct gate
        {
            private:

                // The lower 16 bit of offset (aka pointer)
                uint16_t offset_low__:16;

                // Segment selector
                uint16_t selector__:16;

                // Unused 8 bits
                uint8_t zero__:8;

                // Type of the gate
                uint8_t type__:4;

                // Storage segment
                bool storage_segment__:1;

                // Minimum privilege level required to trigger
                uint8_t privilege_level__:2;

                // Whether the interrupt is available
                bool present__:1;

                // Higher 16 bit of offset
                uint16_t offset_high__:16;

            public:

                /**
                 * @brief Constructs a null IDT gate
                 */
                constexpr gate()
                    : gate(0, 0, 0, 0, false)
                {}

                /**
                 * @brief Constructs an IDT gate
                 *
                 * @details @rst
                 *
                 * This constructor construct a IDT gate with given parameters.
                 * For details of parameters, see Intel manual.
                 *
                 * @endrst
                 *
                 * @param offset offset to interrupt handler
                 * @param selector code
                 * :ref:`segment selector <definition-segment_selector>`
                 * @param type type of the gate
                 * @param privilege_level minimum privilege level required to
                 * trigger the interrupt
                 * @param present whether the interrupt is present
                 * @param storage_segment storage segment
                 */
                constexpr gate(
                    uint32_t offset,
                    uint16_t selector,
                    uint8_t type,
                    uint8_t privilege_level,
                    bool present = true,
                    bool storage_segment = false
                )
                    : offset_low__(0),
                      selector__(selector),
                      zero__(0),
                      type__(type),
                      storage_segment__(storage_segment),
                      privilege_level__(privilege_level),
                      present__(present),
                      offset_high__(0)
                {
                    this->offset(offset);
                }

                /**
                 * @brief Returns the offset of the gate
                 *
                 * @return the offset of the gate
                 */
                constexpr uint32_t offset() const
                {
                    return static_cast<uint32_t>(offset_high__) << 16 |
                        static_cast<uint32_t>(offset_low__);
                }

                /**
                 * @brief Sets the offset of the gate
                 */
                constexpr void offset(uint32_t value)
                {
                    offset_low__ = value & 0xffff;
                    offset_high__ = value >> 16;
                }

                /**
                 * @brief Returns the segment selector of the gate
                 *
                 * @return the segment selector of the gate
                 */
                constexpr uint16_t selector() const
                {
                    return selector__;
                }

                /**
                 * @brief Sets the segment selector of the gate
                 */
                constexpr void selector(uint16_t value)
                {
                    selector__ = value;
                }

                /**
                 * @brief Returns the type of the gate
                 *
                 * @return the type of the gate
                 */
                constexpr uint8_t type() const
                {
                    return type__;
                }

                /**
                 * @brief Sets the type of the gate
                 */
                constexpr void type(uint8_t value)
                {
                    type__ = value;
                }

                /**
                 * @brief Returns the segment storage of the gate
                 *
                 * @return the segment storage of the gate
                 */
                constexpr bool segment_storage() const
                {
                    return storage_segment__;
                }

                /**
                 * @brief Sets the segment storage of the gate
                 */
                constexpr void segment_storage(bool value)
                {
                    storage_segment__ = value;
                }

                /**
                 * @brief Returns the privilege level of the gate
                 *
                 * @return the privilege level of the gate
                 */
                constexpr uint8_t privilege_level() const
                {
                    return privilege_level__;
                }

                /**
                 * @brief Sets the privilege level of the gate
                 */
                constexpr void privilege_level(uint8_t value)
                {
                    privilege_level__ = value & 0b11;
                }

                /**
                 * @brief Returns whether the interrupt is present
                 *
                 * @return whether the interrupt is present
                 */
                constexpr bool present() const
                {
                    return present__;
                }

                /**
                 * @brief Sets whether the interrupt is present
                 */
                constexpr void present(bool value)
                {
                    present__ = value;
                }
        }
#ifndef DOXYGEN_DOCUMENTATION_BUILD
            __attribute__((packed))
#endif
        ;

        /**
         * @brief Flushes the given IDT
         *
         * @param table pointer to the first gate of the IDT
         * @param count size of the table
         */
        void flush(const gate* table, size_t count);

        /**
         * @brief Sets up an IDT
         *
         * @details @rst
         *
         * This function sets up and loads an IDT with 256 entries. If any error
         * occurs, ``nullptr`` is returned.
         *
         * .. warning::
         *
         *      Calling this function will overwrite any IDT returned previously
         *      by this function. Be sure to backup the previous IDT before
         *      calling this function again!
         *
         * @endrst
         *
         * @param flush whether to flush the IDT
         * @param pic whether to initialize the PIC
         * @return pointer to the IDT
         */
        gate* init(bool flush = true, bool pic = true);

        /**
         * @brief Structure for passing information to interrupt handler
         */
        struct registers_t
        {

            /**
             * @brief ``cs`` register, holds code segment selector
             */
            uint16_t cs;

            /**
             * @brief ``ds`` register, holds data segment selector
             */
            uint16_t ds;

            /**
             * @brief ``fs`` register, holds general purpose segment selector
             */
            uint16_t fs;

            /**
             * @brief ``gs`` register, holds general purpose segment selector
             */
            uint16_t gs;

            /**
             * @brief ``ss`` register, holds stack segment selector
             */
            uint16_t ss;

            /**
             * @brief ``eip`` register, aka extended instruction pointer
             */
            uint32_t eip;

            /**
             * @brief ``eflags`` register, aka extended flags
             */
            uint32_t eflags;
        };

        /**
         * @brief Typedef for interrupt handlers
         */
        typedef void(*interrupt_handler)(
            uint8_t interrupt,
            uint8_t code,
            registers_t& registers
        );

        /**
         * @brief Sets the handler for the given interrupt
         *
         * @details @rst
         *
         * This function sets the halder of given interrupt. The handler must
         * take three parameter: interrupt number, error code and reference to a
         * structure holding registers. Error code is zero when the interrupt
         * doesn't push an error code.
         *
         * .. warning::
         *
         *      Modifing the registers may crash the system. Be careful!
         *
         * @endrst
         *
         * @param interrupt interrupt number
         * @param handler handler function takeing interrupt number, error code
         * and registers as parameter
         */
        void set_handler(uint8_t interrupt, interrupt_handler handler);

        /**
         * @brief Returns the handler of the given interupt
         *
         * @return the handler of the given interupt
         */
        interrupt_handler get_handler(uint8_t interrupt);
    }
}
