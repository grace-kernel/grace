/*  vga.h - VGA related C functions

    This file is a part of Grace.
    Copyright (C) 2021 Akib Azmain Turja

    Grace is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Grace is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <stdint.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C"
{
#endif

/**
 * @brief Structure for holding the cursor position
 */
struct vga_curpos
{

    /**
     * @brief The Y coordinate of the cursor
     */
    uint8_t y;

    /**
     * @brief The X coordinate of the cursor
     */
    uint8_t x;
};

/**
 * @brief Enum holding all possible colors in VGA
 */
enum vga_color
{
    vga_color_black           = 0x0,
    vga_color_blue            = 0x1,
    vga_color_green           = 0x2,
    vga_color_cyan            = 0x3,
    vga_color_red             = 0x4,
    vga_color_magenta         = 0x5,
    vga_color_brown           = 0x6,
    vga_color_light_grey      = 0x7,
    vga_color_dark_grey       = 0x8,
    vga_color_light_blue      = 0x9,
    vga_color_light_green     = 0xa,
    vga_color_light_cyan      = 0xb,
    vga_color_light_red       = 0xc,
    vga_color_light_magenta   = 0xd,
    vga_color_light_brown     = 0xe,
    vga_color_white           = 0xf
};

/**
 * @brief Structure for holding the foregroud and backgroud color of a character
 * in VGA
 */
struct vga_color_pair
{

    /**
     * @brief The foreground color of the character
     */
    vga_color fg;

    /**
     * @brief The background color of the character
     */
    vga_color bg;
};

/**
 * @brief Structure for holding the cursor shape
 */
struct vga_cursor_shape
{

    /**
     * @brief Start scanline of the cursor
     */
    uint8_t start;

    /**
     * @brief End scanline of the cursor
     */
    uint8_t end;
};

/**
 * @brief Initialize the VGA library
 *
 * @details @rst
 *
 * This function initializes the VGA library.
 *
 * .. important::
 *
 *      Remember to call this before calling any other VGA functions, doing
 *      otherwise will led to undefined behaviour.
 *
 * .. note::
 *
 *      This function doesn't alter the display, use appropriate functions for
 *      that.
 *
 * @endrst
 */
void vga_init();

/**
 * @brief Clears the screen
 */
void vga_clear();

/**
 * @brief Prints a character on the screen
 */
void vga_putc(char ch);

/**
 * @brief Flushes the display buffer to the screen
 */
void vga_flush();

/**
 * @brief Moves the cursor
 */
void vga_curmove(uint8_t y, uint8_t x);

/**
 * @brief Returns the cursor position
 */
vga_curpos vga_getcurpos();

/**
 * @brief Writes a string with given length to the screen
 */
void vga_write(const char* data, size_t count);

/**
 * @brief Changes the color of characters
 *
 * @details @rst
 *
 * This function changes the color of the characters.
 *
 * .. note::
 *
 *      This doesn't affect the character already written.
 *
 * @endrst
 */
void vga_setcolor(vga_color fg, vga_color bg);

/**
 * @brief Return the color of characters
 */
vga_color_pair vga_getcolor();

/**
 * @brief Sets the cursor size
 *
 * @param start start scanline
 * @param end end scanline
 */
void vga_setcur(uint8_t start, uint8_t end);

/**
 * @brief Returns the cursor size
 */
vga_cursor_shape vga_getcur();

#ifdef __cplusplus
}
#endif
