/*  main.cpp - Main function

    This file is a part of Grace.
    Copyright (C) 2021 Akib Azmain Turja

    Grace is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Grace is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <grace/vga.h>
#include <grace/utils.h>
#include <grace/gdt.hpp>
#include <grace/idt.hpp>

namespace
{
    grace::gdt::entry* gdt;
    grace::idt::gate* idt;

    // Initializes things like GDT and IDT
    void init()
    {
        gdt = grace::gdt::init();
        idt = grace::idt::init();
    }
}

extern "C" void main()
{

    // Setup first
    init();

    vga_init();
    vga_clear();
    vga_write("Hello! Grace is currently a dummy kernel with no functionality, "
              "please\ncontribute to it.\n", 89);
    vga_flush();

    grace_freeze_cpu();
}
