/*  gdt_flush.s - Symbols for flushing GDT for i386 architecture

    This file is a part of Grace.
    Copyright (C) 2021 Akib Azmain Turja

    Grace is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Grace is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

    .type gdt_flush__, @function
    .global gdt_flush__
gdt_flush__:
    mov 4(%esp), %eax
    lgdt (%eax)
    ljmp $0x8, $load_other_segments

load_other_segments:
    mov $0x10, %ax
    mov %ax, %ds
    mov %ax, %fs
    mov %ax, %gs
    mov %ax, %ss
    ret
