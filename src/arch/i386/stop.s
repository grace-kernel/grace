/*  stop.s - Symbols for freezing the CPU on errors for i386 architecture

    This file is a part of Grace.
    Copyright (C) 2021 Akib Azmain Turja

    Grace is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Grace is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// Freezes CPU in case of any fatal non-recoverable error
.section .text
.global stop__
.type stop__, @function
stop__:
    call grace_freeze_cpu

// Set the size of 'stop__' symbol for debugging purposes
.size stop__, . - stop__
