/*  idt_routine_common.cpp - Functions for dispatching interrupts for i386
    architecture

    This file is a part of Grace.
    Copyright (C) 2021 Akib Azmain Turja

    Grace is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Grace is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <grace/idt.hpp>
#include <grace/vga.h>
#include <grace/utils.h>
#include <grace/mboard.h>

namespace
{

    // Structure for holding registers pushed by low-level assembly handler
    struct registers
    {
        uint32_t ds;
        uint32_t edi;
        uint32_t esi;
        uint32_t ebp;
        uint32_t unused__; // Unused
        uint32_t ebx;
        uint32_t edx;
        uint32_t ecx;
        uint32_t eax;
        uint32_t interrupt;
        uint32_t code;
        uint32_t eip;
        uint32_t cs;
        uint32_t eflags;
        uint32_t esp;
        uint32_t ss;
    };

    grace::idt::interrupt_handler handlers[256];
}

namespace grace::idt
{
    void set_handler(uint8_t interrupt, interrupt_handler handler)
    {
        handlers[interrupt] = handler;
    }

    interrupt_handler get_handler(uint8_t interrupt)
    {
        return handlers[interrupt];
    }
}

extern "C" void isr_common__(volatile registers pushed_registers)
{

    // Whether it is a hardware interrupt
    bool is_irq =
        pushed_registers.interrupt >= 0x20 &&
        pushed_registers.interrupt < 0x30;

    // Check if the interrupt is spurious
    if (is_irq)
    {

        // Check if it is a spurious interrupt by the master PIC
        if (pushed_registers.interrupt == 0x7 + 0x20)
        {

            // Get In-Service Register value
            mboard_outb(0x20, 0xb);
            if (!(mboard_inb(0x20) >> 7 & 1))
            {
                return;
            }
        }

        // Check if it is a spurious interrupt by the slave PIC
        else if (pushed_registers.interrupt == 0x7 + 0x28)
        {

            // Get In-Service Register value
            mboard_outb(0xa0, 0xb);
            if (!(mboard_inb(0xa0) >> 7 & 1))
            {

                // Send EOI to the master PIC
                mboard_outb(0x20, 0x20);
                return;
            }
        }
    }

    // Check if a handler is registered for the interrupt
    if (handlers[pushed_registers.interrupt] != nullptr)
    {

        // Setup the structure to pass to the handler
        grace::idt::registers_t registers =
            {
            static_cast<uint16_t>(pushed_registers.cs),
            static_cast<uint16_t>(pushed_registers.ds),
            static_cast<uint16_t>(pushed_registers.ds),
            static_cast<uint16_t>(pushed_registers.ds),
            static_cast<uint16_t>(pushed_registers.cs),
            pushed_registers.eip,
            pushed_registers.eflags
        };

        // Call the handler
        handlers[pushed_registers.interrupt](
            pushed_registers.interrupt,
            pushed_registers.code,
            registers
        );

        // Update registers
        pushed_registers.cs = registers.cs;
        pushed_registers.ds = registers.ds;
        pushed_registers.eip = registers.eip;
        pushed_registers.eflags = registers.eflags;

        // Send EOI signals to PICs if required
        if (is_irq)
        {

            // If the IRQ is from the slave PIC, send EOI to it
            if (pushed_registers.interrupt >= 0x28)
            {
                mboard_outb(0xa0, 0x20);
            }

            // In any case, send EOI to the master PIC
            mboard_outb(0x20, 0x20);
        }
    }

    // Otherwise, show "unhandler interrupt" and halt
    else
    {

        // Required to convert "uint8_t" to hexadecimal number
        char hex_digits[] =
            {
            '0', '1', '2', '3', '4', '5', '6', '7', '8',
            '9', 'a', 'b', 'c', 'd', 'e', 'f'
        };

        // Show unhandled interrupt error
        vga_write("unhandled interrupt: 0x", 23);
        vga_putc(hex_digits[pushed_registers.interrupt >> 4 & 0xf]);
        vga_putc(hex_digits[pushed_registers.interrupt & 0xf]);
        vga_putc('\n');
        vga_flush();

        // We don't know what to do, halting CPU is the best solution
        grace_freeze_cpu();
    }
}
