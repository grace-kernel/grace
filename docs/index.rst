..  Copyright (c)  2021  Akib Azmain Turja.
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3
    or any later version published by the Free Software Foundation;
    with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
    A copy of the license is included in the section entitled "GNU
    Free Documentation License".

Grace Documentation
===================

This documentation is for Grace, an incomplete microkernel under development
designed for being compatible with the GNU Hurd servers and other GNU programs.

Copyright (c)  2021  Akib Azmain Turja.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU
Free Documentation License".

.. toctree::
    :maxdepth: 1
    :caption: Introduction

    Introduction to Grace <intro>
    Setting Up <setup>

.. toctree::
    :maxdepth: 1
    :caption: Developing

    ABI References <abi/index>
    Internal API References <api/index>


.. toctree::
    :maxdepth: 1
    :caption: Miscellaneous

    Definitions <definitions>
    GNU Free Documentation License <licenses/gnu-fdl>
    GNU General Public License <licenses/gnu-gpl>
