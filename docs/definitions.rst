Definitions
===========

This page defines many terms used in this documentation. If you see something
which is ambitious, see this page.

.. _definition-bit:

bit
    "bit" is a data structure which can hold only two values, 0 or 1.

.. _definition-byte:

byte
    "byte" means a data structure of 8 bits.

.. _definition-free:

free
    The term "free" means "freedom", not "free of cost". When referring to cost,
    the term ":ref:`gratis <definition-gratis>`" is used.

.. _definition-gratis:

gratis
    "Gratis" means "free of cost". When the term ":ref:`free <definition-free>`"
    is used, it means "freedom".

.. _definition-hack:
.. _definition-hacking:

hack
    The term "hack" or "hacking" means modifying the behavior or properties of a
    thing (i.e modifying source code of programs for own purposes``. This term
    doesn't means attempting to access a computer maliciously.

hacking
    See :ref:`hack <definition-hack>`.

.. _definition-segment_selector:

segment selector
    The parameter used to set the above register must be 16 bit data
    and have the following format:

    .. code-block:: text

        15  14  13  12  11  10  9   8   7   6   5   4   3   2   1   0
        +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
        |                              Index of segment | T |    PL |
        +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+

    Here:

    * PL means privilege level
    * T means entry type, ``0`` for GDT entry, ``1`` for LDT entry.

.. _definition-word:

word
    The meaning "word" depends on the context. When referring to data, it means
    two bytes of data.
