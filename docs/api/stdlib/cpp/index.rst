..  Copyright (c)  2021  Akib Azmain Turja.
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3
    or any later version published by the Free Software Foundation;
    with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
    A copy of the license is included in the section entitled "GNU
    Free Documentation License".

C++ Standard Library
====================

Grace Internal C++ standard library provides a convenient interface to hack or
extend the kernel using extensions. Note that this C++ library doesn't implement
all the functions specified in the ISO C++ Standard, it implements only a
reasonable amount of functions, classes and other things.
