..  Copyright (c)  2021  Akib Azmain Turja.
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3
    or any later version published by the Free Software Foundation;
    with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
    A copy of the license is included in the section entitled "GNU
    Free Documentation License".

``idt.hpp`` - IDT management functions and class for i386 architecture
======================================================================

.. doxygenstruct:: grace::idt::gate
    :members:

.. doxygenfunction:: grace::idt::init

.. doxygenfunction:: grace::idt::flush

.. doxygenstruct:: grace::idt::registers_t
    :members:

.. doxygentypedef:: grace::idt::interrupt_handler

.. doxygenfunction:: grace::idt::get_handler

.. doxygenfunction:: grace::idt::set_handler
