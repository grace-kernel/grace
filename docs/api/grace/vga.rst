..  Copyright (c)  2021  Akib Azmain Turja.
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3
    or any later version published by the Free Software Foundation;
    with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
    A copy of the license is included in the section entitled "GNU
    Free Documentation License".

``vga.h`` - VGA related C functions
===================================

.. doxygenfunction:: vga_clear

.. doxygenenum:: vga_color

.. doxygenstruct:: vga_color_pair

.. doxygenfunction:: vga_curmove

.. doxygenstruct:: vga_curpos

.. doxygenfunction:: vga_flush

.. doxygenfunction:: vga_getcolor

.. doxygenfunction:: vga_getcurpos

.. doxygenfunction:: vga_init

.. doxygenfunction:: vga_putc

.. doxygenfunction:: vga_setcolor

.. doxygenfunction:: vga_write
