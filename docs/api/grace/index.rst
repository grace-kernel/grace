..  Copyright (c)  2021  Akib Azmain Turja.
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3
    or any later version published by the Free Software Foundation;
    with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
    A copy of the license is included in the section entitled "GNU
    Free Documentation License".

Grace Internal Platform-specific Library
========================================

Grace internal platform-specific library provides a convenient interface to
access the hardware at a low level. It tries to be platform-independent where
appropriate, but doesn't abstract away too much details.

.. toctree::
    :maxdepth: 1

    gdt
    idt
    mboard
    utils
    vga
