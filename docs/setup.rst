..  Copyright (c)  2021  Akib Azmain Turja.
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3
    or any later version published by the Free Software Foundation;
    with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
    A copy of the license is included in the section entitled "GNU
    Free Documentation License".

Setting Up
==========

Grace is intended to be compatible with the GNU tools. So, to build Grace,
you'll need:

*   A supported machine
*   A cross-compiler
*   The source code


Supported architectures
-----------------------

Currently, only the following architectures are supported:

*   i386
*   i486
*   i586
*   i686


Supported ``configure`` options
-------------------------------

The ``configure`` script shipped with Grace distribution supports all standard
options. It also accepts the following options:

*   ``--enable-building-docs``: Whether to build the documentation along with
    the kernel, default to yes.
*   ``--with-docs-theme=[theme]``: The Sphinx to use when building docs,
    defaults to ``sphinx_rtd_theme``.
*   ``DOXYGEN``: Path to ``doxygen`` for build documentations
*   ``SPHINXBUILD``: Path to ``sphinx-build`` for build documentations
*   ``SPHINXFLAGS``: extra flag for ``sphinx-build``


Compiling
---------

Compiling GRACE requires a cross-compiler targeting 'ix86-elf' (preferably
'i686-elf'). See https://wiki.osdev.org/GCC_Cross-Compiler for instructions on
building a cross-compiler.

1.  Download a release or get the latest code with: (use ``git clone`` if you
    want to develop and hack the kernel)

    .. code-block::

        git clone git@gitlab.com:grace-kernel/grace.git

2.  Unpack the archive (if downloaded from releases).

3.  Execute the following: (if cloned the git repository)

    .. code-block::

        autoreconf --install

4.  Create a dedicated directory to the build.

5.  Configure the build system::

        ../configure \
            --host=i686-elf

    The ``--host`` not required if you aren't cross-compiling. Also, if the
    ``configure`` script fails saying the C++ compiler doesn't work, pass this
    to ``configure``::

        CXXFLAGS="-ffreestanding -fno-rtti -fno-exceptions -nostdlib"

6.  Compile with the following command::

        make


Running
-------

After compiling Grace, the kernel will be located in 'src/grace' in the build
directory. QEMU can run Grace directly with any bootloader::

    qemu-system-i386 -kernel src/grace

You can try to boot on a real machine (at your own risk!), but it is very
unlikely that it will work.
